import Vue from 'vue';
import Router from 'vue-router';
import Brands from './views/Brands.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Brands',
      component: Brands,
    },
  ],
});
